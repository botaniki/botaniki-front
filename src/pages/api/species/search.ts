import { searchSpecies } from "../../../lib/trefleClient";
import { NextApiRequest, NextApiResponse } from "next";

export default async function search(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method === "GET") {
    const searchResult = await searchSpecies(req.query.q);
    res.status(200);
    res.setHeader("Content-Type", "application/json");
    res.send(JSON.stringify(searchResult));
  } else {
    res.status(405);
  }
}
