import styles from "../../styles/Search.module.css";
import { GetServerSidePropsResult } from "next";
import { useEffect, useState } from "react";
import { useAutofocus, withInputValue } from "../../lib/inputUtils";
import Link from "next/link";
import { searchSpecies } from "../../lib/trefleClient";
import { Species } from "../../lib/trefle";

type SearchResultPageProps = {
  searchString: string;
  initialResult: Species[];
};

const SearchResultPage = function SearchResultPage({
  searchString,
  initialResult,
}: SearchResultPageProps) {
  const [currentSearchString, changeSearchString] = useState<string>(
    searchString,
  );
  const [result, setResult] = useState(initialResult);
  useEffect(() => {
    if (currentSearchString !== searchString) {
      fetch(`/api/species/search?q=${currentSearchString}`)
        .then((resp) => resp.json())
        .then(setResult);
    }
  }, [currentSearchString]);

  const autofocus = useAutofocus();

  return (
    <>
      <header className={styles.header}>
        <Link href="/">
          <span className={styles.logo}>
            <span className="fas fa-leaf logo" /> Botaniki
          </span>
        </Link>
        <input
          {...autofocus}
          className={styles.searchBar}
          type="text"
          value={currentSearchString}
          onChange={withInputValue(changeSearchString)}
        />
      </header>
      <main className={styles.main}>
        <pre>{JSON.stringify(result, null, 2)}</pre>
      </main>
    </>
  );
};
export default SearchResultPage;

export async function getServerSideProps({
  params,
}): Promise<GetServerSidePropsResult<SearchResultPageProps>> {
  const initialResult = await searchSpecies(params.searchString);

  return {
    props: {
      searchString: params.searchString,
      initialResult,
    },
  };
}
