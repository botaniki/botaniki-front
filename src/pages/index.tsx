import Head from "next/head";
import ExampleLink from "../components/ExampleLink";
import HomeSearchForm from "../components/home/HomeSearchForm";
import HomeLayout from "../components/home/HomeLayout";

const Home = function Home() {
  return (
    <>
      <Head>
        <title>Botaniki</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <HomeLayout>
        <HomeSearchForm />
        <p>
          Examples: <ExampleLink>Tomato</ExampleLink>
        </p>
      </HomeLayout>
    </>
  );
};

export default Home;
