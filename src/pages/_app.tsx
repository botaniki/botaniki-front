import "../styles/globals.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import styles from "../styles/App.module.css";

const App = function App({ Component, pageProps }) {
  return (
    <div className={styles.container}>
      <Component {...pageProps} />

      <footer className={styles.footer}>
        <span>
          I am{" "}
          <a
            href="https://gitlab.com/botaniki"
            target="_blank"
            rel="noopener noreferrer"
          >
            open source&nbsp;
            <span className="fab fa-gitlab" />
          </a>
        </span>
      </footer>
    </div>
  );
};

export default App;
