import { Species } from "../../lib/trefle";
import { GetStaticPaths, GetStaticProps } from "next";
import { oneSpecies } from "../../lib/trefleClient";

type SpeciesPageProps = {
  species: Species | null;
};

const SpeciesDetails = function SpeciesDetails({ species }: SpeciesPageProps) {
  return <pre>{JSON.stringify(species, null, 2)}</pre>;
};

export default SpeciesDetails;

export const getStaticProps: GetStaticProps<SpeciesPageProps> = async function (
  context,
) {
  let species: Species | null = null;

  // Can't be false because next would generate a 404
  if (context.params && typeof context.params.speciesSlug === "string") {
    const { speciesSlug } = context.params;
    species = await oneSpecies(speciesSlug);
  }

  return {
    props: {
      species,
    },
    revalidate: 3600,
  };
};

export const getStaticPaths: GetStaticPaths = async function () {
  return {
    paths: [],
    fallback: true,
  };
};
