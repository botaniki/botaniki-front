import Link from "next/link";

type ExampleLinkChildren = { children: string };

const ExampleLink = function ({ children }: ExampleLinkChildren) {
  return <Link href={`/search/${children}`}>{children}</Link>;
};

export default ExampleLink;
