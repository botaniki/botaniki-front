import { useState } from "react";
import { useRouter } from "next/router";
import { useAutofocus, withInputValue } from "../../lib/inputUtils";
import styles from "./HomeSearchForm.module.css";

const HomeSearchForm = function HomeSearchForm() {
  const [searchString, setSearchString] = useState("");
  const router = useRouter();

  function search(e) {
    e.preventDefault();
    router.push(`/search/${searchString}`);
  }

  const autofocus = useAutofocus();

  return (
    <div className={styles.searchBar}>
      <form onSubmit={search}>
        <input
          {...autofocus}
          value={searchString}
          onChange={withInputValue(setSearchString)}
          type="text"
          placeholder="Search for a plant..."
        />
        <button type="submit">Go</button>
      </form>
    </div>
  );
};

export default HomeSearchForm;
