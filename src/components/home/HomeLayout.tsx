import styles from "./HomeLayout.module.css";

const HomeLayout = function HomeLayout({ children }) {
  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.title}>
          <span className="fas fa-leaf logo" /> Botaniki
        </h1>

        <p className={styles.description}>
          A powerful plant search engine for gardeners
        </p>
      </header>

      <main className={styles.main}>{children}</main>
    </>
  );
};

export default HomeLayout;
