/* eslint-disable camelcase */

export interface Species {
  id: number;
  slug: string;
  scientific_name: string;
  common_name: string;
  image_url: string;
}
