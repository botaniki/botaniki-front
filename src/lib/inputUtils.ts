import { ChangeEventHandler, MutableRefObject, useEffect, useRef } from "react";

type ValueHandler = (string) => any;

export function withInputValue(
  valueHandler: ValueHandler,
): ChangeEventHandler<HTMLInputElement> {
  return (event) => valueHandler(event.target.value);
}

export type AutofocusProps = {
  ref: MutableRefObject<HTMLInputElement | null>;
  autoFocus: true;
};
export function useAutofocus(): AutofocusProps {
  const element = useRef<HTMLInputElement | null>(null);
  useEffect(() => {
    if (element.current != null) {
      element.current.focus();
      // put the cursor at the end if there is already a value
      const v = element.current.value;
      element.current.value = "";
      element.current.value = v;
    }
  }, []);
  return { ref: element, autoFocus: true };
}
