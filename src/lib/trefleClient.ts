import fetch from "isomorphic-unfetch";
import trefleApiKey from "./trefleApiKey";
import { Species } from "./trefle";
import pick from "lodash/fp/pick";
import map from "lodash/fp/map";

const speciesFields = [
  "id",
  "slug",
  "scientific_name",
  "image_url",
  "common_name",
];
const pickSpeciesFields: any = pick(speciesFields);

export function oneSpecies(slug: string): Promise<Species> {
  return fetch(`https://trefle.io/api/v1/species/${slug}?token=${trefleApiKey}`)
    .then((resp) => resp.json())
    .then((body) => body.data)
    .then(pickSpeciesFields);
}

export function searchSpecies(searchString): Promise<Species[]> {
  return fetch(
    `https://trefle.io/api/v1/species/search?token=${trefleApiKey}&q=${searchString}`,
  )
    .then((resp) => resp.json())
    .then((body) => body.data)
    .then(map(pickSpeciesFields));
}
